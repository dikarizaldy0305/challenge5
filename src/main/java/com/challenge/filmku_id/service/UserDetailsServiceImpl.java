package com.challenge.filmku_id.service;

import com.challenge.filmku_id.model.UserDetailsImpl;
import com.challenge.filmku_id.model.Users;
import com.challenge.filmku_id.repository.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {
    @Autowired
    UsersRepository usersRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Users user = (Users) usersRepository.findByUsername(username);
        return UserDetailsImpl.build(user);
    }
}
