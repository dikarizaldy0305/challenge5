package com.challenge.filmku_id.service;

import com.challenge.filmku_id.model.Users;
import com.challenge.filmku_id.repository.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import org.springframework.util.ResourceUtils;
import org.springframework.util.StringUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ReportsService {

    @Autowired
    UsersRepository userRepo;

    private JasperPrint getJasperPrint(List<Map<String, Object>> userCollection, String resourceLocation) throws FileNotFoundException, JRException {
        File file = ResourceUtils.getFile(resourceLocation);
        JasperReport jasperReport = JasperCompileManager
                .compileReport(file.getAbsolutePath());
        JRBeanCollectionDataSource dataSource = new
                JRBeanCollectionDataSource(userCollection);
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("createdBy","Andika Rizaldy");

        JasperPrint jasperPrint = JasperFillManager
                .fillReport(jasperReport,parameters,dataSource);

        return jasperPrint;
    }

    private Path getUploadPath(String fileFormat, JasperPrint jasperPrint, String fileName) throws IOException, JRException {
        String uploadDir = StringUtils.cleanPath("src/main/resources");
        Path uploadPath = Paths.get(uploadDir);
        if (!Files.exists(uploadPath)){
            Files.createDirectories(uploadPath);
        }
        //generate the report and save it in the just created folder
        if (fileFormat.equalsIgnoreCase("pdf")){
            JasperExportManager.exportReportToPdfFile(
                    jasperPrint, uploadPath+fileName
            );
        }

        return uploadPath;
    }

    private String getPdfFileLink(String uploadPath){
        return uploadPath+"/"+"invoice.pdf";
    }

    public String generateReport(LocalDate localDate, String fileFormat){
        try {
            List<Map<String, Object>> userDetail = new ArrayList<>();
            Map<String, Object> data = new HashMap<>();
            Users user = (Users) userRepo.findByUsername("Dika");
            data.put("username", user.getUsername());
            data.put("email", user.getEmail());
            userDetail.add(data);
            //load the file and compile it
            String resourceLocation = "classpath:Tes.jrxml";
            JasperPrint jasperPrint = getJasperPrint(userDetail,resourceLocation);
            //create a folder to store the report
            String fileName = "/"+"Invoice.pdf";
            Path uploadPath = getUploadPath(fileFormat, jasperPrint, fileName);
            return getPdfFileLink(uploadPath.toString());
        } catch(Exception e) {
            e.printStackTrace();
        }

        //create a private method that returns the link to the specific pdf file
        return null;
    }

    //    @Override
    public List<Users> findAllProducts() {
        return userRepo.findAll();
    }

}
