package com.challenge.filmku_id.enumeration;

public enum ERole {
    ADMIN, CUSTOMER
}
