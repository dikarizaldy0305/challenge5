package com.challenge.filmku_id.repository;


import com.challenge.filmku_id.model.Roles;
import com.challenge.filmku_id.enumeration.ERole;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RoleRepository extends JpaRepository<Roles, Integer> {

    Optional<Roles> findByName(ERole name);
}
