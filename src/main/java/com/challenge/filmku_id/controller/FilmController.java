package com.challenge.filmku_id.controller;

import com.challenge.filmku_id.model.Films;
import com.challenge.filmku_id.model.Schedules;
import com.challenge.filmku_id.service.FilmService;
import com.challenge.filmku_id.service.ScheduleService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import io.swagger.v3.oas.annotations.tags.Tags;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Tag(name="Films", description = "API for processing with Films entity")
@RestController
@RequestMapping("/films")
public class FilmController {

    @Autowired
    private FilmService filmService;

    @Autowired
    private ScheduleService scheduleService;

    @Operation(summary = "Add a new film to the cinema")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "221", description = "Success added a new film", content ={@Content(schema = @Schema(example = "Add Film Success!"))}),
    })
    @PostMapping("/add-film")
    public ResponseEntity addFilm(@Schema(example = "{" +
            "\"filmCode\":\"A005\"," +
            "\"namaFilm\":\"Take Me Home\"," +
            "\"statusTayang\":\"Tidak Tayang\"" +
            "}")
                                  @RequestBody Map<String, Object> film){
        filmService.saveFilm(film.get("filmCode").toString(), film.get("namaFilm").toString(), film.get("statusTayang").toString());

        Map<String, Object> responBody = new HashMap<>();
        responBody.put("filmCode",film.get("filmCode"));
        responBody.put("namaFilm",film.get("namaFilm"));
        responBody.put("statusTayang",film.get("statusTayang"));

        MultiValueMap<String, String> headers = new HttpHeaders();
        headers.put("Binar", Arrays.asList("Test"));
        return new ResponseEntity(responBody, headers, 221);

    }

    @Operation(summary = "Add schedule to schedule Entity")
    @ApiResponse(responseCode = "231", description = "Add Schedule Success!", content ={@Content(schema = @Schema(example = "Add Schedule Success!"))})
    @PostMapping("/add-schedule")
    public String addSchedule(@RequestBody Schedules schedule) {
        scheduleService.saveSchedule(schedule.getHargaTiket(),schedule.getJamMulai(),schedule.getJamSelesai(),
                schedule.getTanggalTayang(),schedule.getFilmId().getFilmId());
        return "Add Schedule Success!";
    }

    @Operation(summary = "Update film in film Entity")
    @ApiResponse(responseCode = "222", description = "Success updated a film", content ={@Content(schema = @Schema(example = "Update Film Success!"))})
    @PutMapping("/update-film")
    public ResponseEntity updateFilm(@Schema(example = "{" +
            "\"filmCode\":\"A005\"," +
            "\"namaFilm\":\"Take Me Home\"," +
            "\"statusTayang\":\"Tidak Tayang\"," +
            "\"filmId\":\"3\"" +
            "}")
                                     @RequestBody Map<String, Object> film){
        filmService.updateFilm(film.get("filmCode").toString(), film.get("namaFilm").
                toString(), film.get("statusTayang").toString(), Integer.valueOf(film.get("filmId").toString()));

        Map<String, Object> responBody = new HashMap<>();
        responBody.put("filmCode",film.get("filmCode"));
        responBody.put("namaFilm",film.get("namaFilm"));
        responBody.put("statusTayang",film.get("statusTayang"));
        responBody.put("filmId",film.get("filmId"));

        MultiValueMap<String, String> headers = new HttpHeaders();
        headers.put("Binar", Arrays.asList("Test"));
        return new ResponseEntity(responBody, headers, 222);

    }

    @Operation(summary = "Delete Film in film Entity")
    @ApiResponse(responseCode = "223", description = "Success deleted a film")
    @DeleteMapping("/delete-film")
    public void deleteFilm(@PathVariable ("filmId") Integer filmId){
        filmService.deleteFilm(filmId);
    }

    @Operation(summary = "Show all movies")
    @ApiResponse(responseCode = "224", description = "\"Success show films")
    @GetMapping("/get-all-film")
    public void allFilm(){
        List<Films> filmsList = filmService.filmsList();
        filmsList.forEach(films -> {
            if(films.getStatusTayang().equals("Sedang Tayang")){
                System.out.println("nama film : "+films.getNamaFilm()+"\ncode film : "+films.getFilmCode()+
                        "\nstatus : "+films.getStatusTayang()+"\n");
            }
        });
    }

    @ApiResponse(responseCode = "225", description = "Success show schedules from a film")
    @Operation (summary = "Show schedules on certain film")
    @GetMapping(value = "/get-by-nama-film/{namaFilm}")
    public ResponseEntity getByNamaFilm(@PathVariable("namaFilm") String namaFilm) {
        List<Schedules> films = scheduleService.getByNamaFilm(namaFilm);
        System.out.println("Nama Film\t\tTanggal Tayang\t\tJam Mulai\t\tJam Selesai\t\tHarga Tiket");
        films.forEach(flm-> {
            System.out.println(flm.getFilmId().getNamaFilm()+"\t\t\t"+flm.getTanggalTayang()+"\t\t\t"+flm.getJamMulai()+
                    "\t\t\t"+flm.getJamSelesai()+"\t\t\tRp. "+flm.getHargaTiket());
        });

        //custom response body
        Map<String, Object> respBody = new HashMap<>();
        respBody.put("Nama Film","Dadi Ganteng");
        respBody.put("Tanggal Tayang", "22-4-2025");


        //custome response headers
        MultiValueMap<String,String> headers = new HttpHeaders();
        headers.put("Binar", Arrays.asList("BEJ-1"));
        headers.set("Pasti Bisa", "timeout=75000");
        return new ResponseEntity(respBody, headers, 225);
    }

}